<?php

namespace WPDesk\FedexProShippingService;

use Psr\Log\LoggerInterface;
use WPDesk\AbstractShipping\Rate\ShipmentRating;
use WPDesk\AbstractShipping\Settings\SettingsValues;
use WPDesk\AbstractShipping\Shipment\Shipment;
use WPDesk\AbstractShipping\Shop\ShopSettings;
use WPDesk\FedexProShippingService\FedexApi\FedexProRateRequestBuilder;
use WPDesk\FedexShippingService\Exception\CurrencySwitcherException;
use WPDesk\FedexShippingService\FedexApi\FedexProRateCurrencyFilter;
use WPDesk\FedexShippingService\FedexApi\FedexRateRequestBuilder;
use WPDesk\FedexShippingService\FedexShippingService;

/**
 * FedEx PRO main shipping class injected into WooCommerce shipping method.
 *
 * @package WPDesk\FedexShippingService
 */
class FedexProShippingService extends FedexShippingService {

	/** @var ShopSettings */
	private $shop_settings;

	/**
	 * .
	 *
	 * @param LoggerInterface $logger Logger.
	 * @param ShopSettings $shop_settings Shop settings.
	 */
	public function __construct( LoggerInterface $logger, ShopSettings $shop_settings ) {
		parent::__construct( $logger, $shop_settings );
		$this->shop_settings = $shop_settings;
	}

	/**
	 * Get settings
	 *
	 * @return FedexProSettingsDefinition
	 */
	public function get_settings_definition() {
		return new FedexProSettingsDefinition( parent::get_settings_definition() );
	}

	/**
	 * Verify currency.
	 *
	 * @param string $default_shop_currency Shop currency.
	 * @param string $checkout_currency Checkout currency.
	 *
	 * @return void
	 * @throws CurrencySwitcherException .
	 */
	protected function verify_currency( $default_shop_currency, $checkout_currency ) {
		// Currently do nothing. PRO version supports multicurrency.
	}

	/**
	 * Creates rate filter by currency.
	 *
	 * @param ShipmentRating $rating .
	 *
	 * @return FedexProRateCurrencyFilter .
	 */
	protected function create_filter_rates_by_currency( ShipmentRating $rating ) {
		return new FedexProRateCurrencyFilter( $rating, $this->shop_settings );
	}

	/**
	 * Create rate request builder.
	 *
	 * @param SettingsValues $settings .
	 * @param Shipment       $shipment .
	 * @param ShopSettings   $shop_settings .
	 *
	 * @return FedexRateRequestBuilder
	 */
	protected function create_rate_request_builder( SettingsValues $settings, Shipment $shipment, ShopSettings $shop_settings ) {
		return new FedexProRateRequestBuilder( $settings, $shipment, $shop_settings );
	}

}

<?php
/**
 * Settings definition.
 *
 * @package WPDesk\FedexProShippingService;
 */

namespace WPDesk\FedexProShippingService;

use UpsProVendor\WPDesk\UpsProShippingService\CutoffTime\CutoffTimeSettingsDefinitionDecorator;
use UpsProVendor\WPDesk\UpsProShippingService\LeadTime\LeadTimeSettingsDefinitionDecorator;
use WPDesk\AbstractShipping\Exception\SettingsFieldNotExistsException;
use WPDesk\AbstractShipping\Settings\SettingsDefinition;
use WPDesk\AbstractShipping\Settings\SettingsValues;
use WPDesk\FedexProShippingService\CustomOrigin\CustomOriginSettingsDefinitionDecorator;
use WPDesk\FedexProShippingService\DatesAndTimes\DatesAndTimesSettingsDefinitionDecorator;
use WPDesk\FedexProShippingService\EstimatedDelivery\EstimatedDeliverySettingsDefinitionDecorator;
use WPDesk\FedexProShippingService\HandlingFees\HandlingFeesSettingsDefinitionDecorator;
use WPDesk\FedexProShippingService\MaximumTransitTime\MaximumTransitTimeSettingsDefinitionDecorator;
use WPDesk\FedexProShippingService\RateCurrency\RateCurrencySettingsDefinitionDecorator;
use WPDesk\FedexShippingService\FedexSettingsDefinition;

/**
 * Settings definitions.
 */
class FedexProSettingsDefinition extends SettingsDefinition {

	/**
	 * Fedex settings definition.
	 *
	 * @var FedexSettingsDefinition
	 */
	private $fedex_settings_definition;

	/**
	 * FedexProSettingsDefinition constructor.
	 *
	 * @param FedexSettingsDefinition $fedex_settings_definition Fedex settings definition.
	 */
	public function __construct( FedexSettingsDefinition $fedex_settings_definition ) {
		$fedex_settings_definition = new HandlingFeesSettingsDefinitionDecorator( $fedex_settings_definition );
		$fedex_settings_definition = new RateCurrencySettingsDefinitionDecorator( $fedex_settings_definition );
		$fedex_settings_definition = new CustomOriginSettingsDefinitionDecorator( $fedex_settings_definition );
		$fedex_settings_definition = new DatesAndTimesSettingsDefinitionDecorator( $fedex_settings_definition );
		$fedex_settings_definition = new EstimatedDeliverySettingsDefinitionDecorator( $fedex_settings_definition );
		$fedex_settings_definition = new MaximumTransitTimeSettingsDefinitionDecorator( $fedex_settings_definition );
		$fedex_settings_definition = new LeadTimeSettingsDefinitionDecorator( $fedex_settings_definition );
		$fedex_settings_definition = new CutoffTimeSettingsDefinitionDecorator( $fedex_settings_definition );
		$this->fedex_settings_definition = $fedex_settings_definition;
	}

	/**
	 * Get form fields.
	 *
	 * @return array
	 *
	 * @throws SettingsFieldNotExistsException .
	 */
	public function get_form_fields() {
		return $this->fedex_settings_definition->get_form_fields();
	}

	/**
	 * Validate settings.
	 *
	 * @param SettingsValues $settings Settings.
	 *
	 * @return bool
	 */
	public function validate_settings( SettingsValues $settings ) {
		return $this->fedex_settings_definition->validate_settings( $settings );
	}
}

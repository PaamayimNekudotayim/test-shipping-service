<?php
/**
 * Request modifier for estimated delivery.
 *
 * @package WPDesk\FedexProShippingService\EstimatedDelivery
 */
namespace WPDesk\FedexProShippingService\EstimatedDelivery;


use FedEx\RateService\ComplexType\RateRequest;
use WPDesk\AbstractShipping\Shipment\Shipment;
use WPDesk\FedexProShippingService\FedexApi\FedexRateRequestModifier;

class EstimatedDeliveryRequestModifier implements FedexRateRequestModifier {
	/**
	 * Delivery dates.
	 *
	 * @var string
	 */
	private $delivery_dates;
	/**
	 * WooCommerce shipment.
	 *
	 * @var Shipment
	 */
	private $shipment;
	/**
	 * EstimatedDeliveryRequestModifier constructor.
	 *
	 * @param string   $delivery_dates .
	 * @param Shipment $shipment Shipment.
	 */
	public function __construct( $delivery_dates, Shipment $shipment ) {
		$this->delivery_dates = $delivery_dates;
		$this->shipment = $shipment;
	}

	/**
	 * Modify rate request.
	 *
	 * @param RateRequest $request
	 *
	 * @throws \Exception .
	 */
	public function modify_rate_request( RateRequest $request ) {
		// TODO: Implement modify_rate_request() method.
	}
}

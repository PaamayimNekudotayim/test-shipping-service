<?php

namespace WPDesk\FedexShippingService\FedexApi;

use WPDesk\AbstractShipping\Rate\ShipmentRating;
use WPDesk\AbstractShipping\Rate\SingleRate;
use WPDesk\AbstractShipping\Shop\ShopSettings;
use WPDesk\FedexProShippingService\Exception\ProNoRatesInCurrencyInRatingsException;
use WPDesk\FedexShippingService\Exception\NoRatesInCurrencyInRatingsException;

/**
 * Can filter rates by currency.
 *
 * @package WPDesk\FedexShippingService\FedexApi
 */
class FedexProRateCurrencyFilter extends FedexRateCurrencyFilter  {

	/** @var ShopSettings */
	private $shop_settings;

	/**
	 * .
	 *
	 * @param ShipmentRating $rating .
	 * @param ShopSettings $shop_settings
	 */
	public function __construct( ShipmentRating $rating, ShopSettings $shop_settings ) {
		$this->shop_settings = $shop_settings;
		parent::__construct( $rating, $shop_settings );
	}

	/**
	 * Get filtered ratings.
	 *
	 * @return SingleRate[]
	 */
	public function get_ratings() {
		try {
			return parent::get_ratings();
		} catch ( NoRatesInCurrencyInRatingsException $e ) {
			throw new ProNoRatesInCurrencyInRatingsException( $this->shop_settings );
		}
	}

}

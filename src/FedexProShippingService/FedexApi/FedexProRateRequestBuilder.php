<?php
/**
 * Fedex API: Build request.
 *
 * @package WPDesk\FedexProShippingService\FedexApi
 */

namespace WPDesk\FedexProShippingService\FedexApi;

use FedEx\RateService\ComplexType\RateRequest;
use WPDesk\AbstractShipping\Settings\SettingsValues;
use WPDesk\AbstractShipping\Shipment\Shipment;
use WPDesk\AbstractShipping\Shop\ShopSettings;
use WPDesk\FedexProShippingService\EstimatedDelivery\EstimatedDeliveryRequestModifier;
use WPDesk\FedexProShippingService\EstimatedDelivery\EstimatedDeliverySettingsDefinitionDecorator;
use WPDesk\FedexProShippingService\RateCurrency\RateCurrencyRequestModifier;
use WPDesk\FedexProShippingService\RateCurrency\RateCurrencySettingsDefinitionDecorator;
use WPDesk\FedexShippingService\FedexApi\FedexRateRequestBuilder;

/**
 * Build request for Fedex rate
 */
class FedexProRateRequestBuilder extends FedexRateRequestBuilder {

	/**
	 * @var FedexRateRequestModifier[]
	 */
	private $rate_request_modifiers = array();

	/**
	 * Settings values.
	 *
	 * @var SettingsValues
	 */
	private $settings;

	/**
	 * WooCommerce shipment.
	 *
	 * @var Shipment
	 */
	private $shipment;

	/**
	 * Shop settings.
	 *
	 * @var ShopSettings
	 */
	private $shop_settings;

	/**
	 * FedexProRateRequestBuilder constructor.
	 *
	 * @param SettingsValues $settings Settings.
	 * @param Shipment       $shipment Shipment.
	 * @param ShopSettings   $shop_settings Helper.
	 */
	public function __construct( SettingsValues $settings, Shipment $shipment, ShopSettings $shop_settings ) {
		parent::__construct( $settings, $shipment, $shop_settings );
		$this->settings      = $settings;
		$this->shipment      = $shipment;
		$this->shop_settings = $shop_settings;
		$this->init_modifiers();
	}

	/**
	 * Init modifiers.
	 */
	private function init_modifiers() {
		$rate_currency = $this->settings->get_value(
			RateCurrencySettingsDefinitionDecorator::OPTION_RATE_CURRENCY,
			RateCurrencySettingsDefinitionDecorator::OPTION_RATE_CURRENCY_DEFAULT
		);
		$delivery_dates = $this->settings->get_value(EstimatedDeliverySettingsDefinitionDecorator::OPTION_DELIVERY_DATES, EstimatedDeliverySettingsDefinitionDecorator::OPTION_NONE);
		$this->rate_request_modifiers[] = new RateCurrencyRequestModifier( $rate_currency, $this->shop_settings->get_default_currency() );
		$this->rate_request_modifiers[] = new EstimatedDeliveryRequestModifier( $delivery_dates, $this->shipment );
	}

	/**
	 * Build request.
	 *
	 * @return RateRequest
	 */
	public function build_request() {
		$request = parent::build_request();
		foreach ( $this->rate_request_modifiers as $modifier ) {
			$modifier->modify_rate_request( $request );
		}
		return $request;
	}

}

<?php
/**
 * Decorator for rate currency.
 *
 * @package WPDesk\FedexProShippingService\RateCurrency
 */

namespace WPDesk\FedexProShippingService\RateCurrency;

use WPDesk\AbstractShipping\Settings\DefinitionModifier\SettingsDefinitionModifierAfter;
use WPDesk\AbstractShipping\Settings\SettingsDefinition;
use WPDesk\FedexShippingService\FedexSettingsDefinition;

/**
 * Can decorate settings for rate currency field.
 */
class RateCurrencySettingsDefinitionDecorator extends SettingsDefinitionModifierAfter {

	const OPTION_RATE_CURRENCY = 'rate_currency';

	const OPTION_RATE_CURRENCY_DEFAULT = 'no';

	public function __construct( SettingsDefinition $fedex_settings_definition ) {
		parent::__construct(
			$fedex_settings_definition,
			FedexSettingsDefinition::FIELD_REQUEST_TYPE,
			self::OPTION_RATE_CURRENCY,
			array(
				'title'       => __( 'Rate Currency', 'fedex-pro-shipping-service' ),
				'type'        => 'checkbox',
				'label'       => __( 'Enable to get rates in shop\'s set currency', 'fedex-pro-shipping-service' ),
				'description' => __( 'Select this option if you sell in other currency than it is set on your FedEx account. FedEx account\'s currency is used by default.', 'fedex-pro-shipping-service' ),
				'desc_tip'    => true,
				'default'     => self::OPTION_RATE_CURRENCY_DEFAULT,
			)
		);
	}

}
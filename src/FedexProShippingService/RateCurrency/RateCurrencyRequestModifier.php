<?php
/**
 * Request modifier for rate currency.
 *
 * @package WPDesk\FedexProShippingService\RateCurrency
 */

namespace WPDesk\FedexProShippingService\RateCurrency;

use FedEx\RateService\ComplexType\RateRequest;
use FedEx\RateService\SimpleType\RateRequestType;
use WPDesk\FedexProShippingService\FedexApi\FedexRateRequestModifier;
use WPDesk\FedexShippingService\FedexApi\FedexRequestManipulation;

/**
 * Can modify request for cutoff time.
 */
class RateCurrencyRequestModifier implements FedexRateRequestModifier {

	/**
	 * Rate currency setting.
	 *
	 * @var string
	 */
	private $rate_currency;

	/** @var string */
	private $shop_default_currency;

	/**
	 * .
	 *
	 * @param string $rate_currency .
	 * @param string $shop_default_currency;
	 */
	public function __construct( $rate_currency, $shop_default_currency ) {
		$this->rate_currency = $rate_currency;
		$this->shop_default_currency = $shop_default_currency;
	}

	/**
	 * Modify rate request.
	 *
	 * @param RateRequest $request
	 */
	public function modify_rate_request( RateRequest $request ) {
		if ( 'yes' === $this->rate_currency ) {
			$request->RequestedShipment->PreferredCurrency = FedexRequestManipulation::convert_currency_to_fedex( $this->shop_default_currency );
			$request->RequestedShipment->RateRequestTypes[] = RateRequestType::_PREFERRED;
		}
	}

}